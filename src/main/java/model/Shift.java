package model;

public class Shift {
  public static final String dayNames[] = {"MONDAY", "TUESDAY",  "WEDNESDAY", "THURSDAY",
                                           "FRIDAY", "SATURDAY", "SUNDAY"};
  String name;

  public String getShiftName() { return name; }
}
