package model;

import com.fasterxml.uuid.*;
import java.util.ArrayList;
import java.util.UUID;

public class Profession extends Model {
  private UUID id;
  private String name;
  private ArrayList<ProfessionShift> shifts;

  public Profession(String name, ArrayList<ProfessionShift> shifts) {
    this.id = Generators.randomBasedGenerator().generate();
    this.name = name;
    this.shifts = shifts;
  }

  public Profession(UUID id, String name, ArrayList<ProfessionShift> shifts) {
    this.id = id;
    this.name = name;
    this.shifts = shifts;
  }

  public UUID getId() { return id; }

  public String getName() { return name; }

  public ArrayList<ProfessionShift> getShifts() { return shifts; }
}
