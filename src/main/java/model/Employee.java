package model;

import com.fasterxml.uuid.*;
import java.util.UUID;

public class Employee extends Model {
  private UUID id, profession;
  private String fname, lname;
  private int weeklyHours;

  public Employee(UUID profession, String firstName, String lastName, int weeklyHours) {
    this.id = Generators.randomBasedGenerator().generate();
    this.profession = profession;
    this.fname = firstName;
    this.lname = lastName;
    this.weeklyHours = weeklyHours;
  }

  public Employee(UUID id, UUID profession, String firstName, String lastName, int weeklyHours) {
    this.id = id;
    this.profession = profession;
    this.fname = firstName;
    this.lname = lastName;
    this.weeklyHours = weeklyHours;
  }

  public UUID getId() { return id; }

  public UUID getProfession() { return profession; }

  public String getFirstName() { return fname; }

  public String getLastName() { return lname; }

  public int getWeeklyHours() { return weeklyHours; }
}
