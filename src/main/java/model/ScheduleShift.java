package model;

import java.util.ArrayList;

public class ScheduleShift extends Shift {
  int month, week, day;
  ArrayList<Employee> employees;

  public ScheduleShift(int month, int week, int day, String name) {
    this.month = month;
    this.week = week;
    this.day = day;
    this.name = name;
    this.employees = new ArrayList<>();
  }

  public void addEmployee(Employee employee) { employees.add(employee); }

  public int getMonth() { return month; }

  public String getDayName() { return dayNames[day - (week * 7)]; }

  public ArrayList<Employee> getEmployees() { return employees; }
}
