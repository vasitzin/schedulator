package model;

import com.fasterxml.uuid.*;
import java.util.ArrayList;
import java.util.UUID;

public class Bussiness extends Model {
  private UUID id;
  private String name;
  private ArrayList<Profession> professions;

  public Bussiness(String name, ArrayList<Profession> professions) {
    this.id = Generators.randomBasedGenerator().generate();
    this.name = name;
    this.professions = professions;
  }

  public Bussiness(UUID id, String name, ArrayList<Profession> professions) {
    this.id = id;
    this.name = name;
    this.professions = professions;
  }

  public UUID getId() { return id; }

  public String getName() { return name; }

  public ArrayList<Profession> getProfessions() { return professions; }
}
