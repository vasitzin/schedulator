package model;

import com.fasterxml.uuid.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class Model {
  private UUID id;

  public static <T> ArrayList<T> sortById(List<UUID> ids, ArrayList<T> array) {
    List<UUID> oldIds = ids;
    Collections.sort(ids, new UUIDComparator());

    T swap;
    for (int i = 0; i < array.size() - 1; ++i) {
      String oldIdStr = oldIds.get(i).toString();
      for (int j = 0; j < ids.size() - 1; ++j) {
        String idStr = ids.get(j).toString();
        if (oldIdStr == idStr) {
          swap = array.get(j);
          array.set(j, array.get(i));
          array.set(i, swap);
          break;
        }
      }
    }
    return array;
  }

  public UUID getId() { return id; }
}
