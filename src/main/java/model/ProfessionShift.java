package model;

public class ProfessionShift extends Shift {
  int positions, hours;

  public ProfessionShift(String name, int positions, int hours) {
    this.name = name;
    this.positions = positions;
    this.hours = hours;
  }

  public int getPositions() { return positions; }

  public int getHours() { return hours; }
}
