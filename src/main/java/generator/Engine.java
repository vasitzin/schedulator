package generator;

import java.util.ArrayList;
import java.util.UUID;
import model.Bussiness;
import model.Employee;
import model.Profession;
import model.ProfessionShift;
import model.ScheduleShift;
import parser.EmployeeParser;

/**
 * You are not supposed to use this directly, extend it instead.
 */
class Engine {

  protected ArrayList<Employee> employees = new EmployeeParser().parse(); // Get employees from file
  protected ArrayList<ScheduleShift> schedule = new ArrayList<>();        // init schedule array

  // Globals for use in rule
  protected Bussiness bussiness;
  protected UUID curProfessionId;
  protected ArrayList<ProfessionShift> curProfessionShifts = new ArrayList<>();
  protected Employee curEmployee;
  protected String curShiftName;
  protected ProfessionShift curProfShift;
  protected ScheduleShift curShift, prevShift;
  protected ArrayList<ScheduleShift> thisDayShifts = new ArrayList<>();
  protected ArrayList<ScheduleShift> thisWeekShifts = new ArrayList<>();

  /**
   * Generates the schedule for given amount of days
   *
   * @param days the number of days as an integer
   * @return schedule as ArrayList of ScheduleShift objects
   */
  public ArrayList<ScheduleShift> generate(int days, Bussiness bussiness) {
    this.bussiness = bussiness;
    schedule.add(
        new ScheduleShift(-1, -1, -1, "")); // adding dummy ScheduleShift obj for shits and giggles

    for (int day = 0; day < days; ++day) {
      employees = new EmployeeParser().shuffleEmployees(employees);    // shuffle employees
      thisDayShifts = new ArrayList<>();                               // reset the shift...
      if (dayOfWeek(day) == 0) { thisWeekShifts = new ArrayList<>(); } // ...arrays each day

      // begin generation for each profession
      for (Profession profession : bussiness.getProfessions()) {
        this.curProfessionId = profession.getId();
        this.curProfessionShifts = profession.getShifts();
        for (ProfessionShift profShift : curProfessionShifts) {               //
          curProfShift = profShift;                                           // keep profs shift
          curShiftName = profShift.getShiftName();                            // keep shift name
          curShift = new ScheduleShift(day / 28, day / 7, day, curShiftName); // make new shift
          prevShift = schedule.get(schedule.size() - 1);                      // keep previous
          for (Employee employee : employees) {                               //
            this.curEmployee = employee;                                      // fill with employees
            if (validation()) curShift.addEmployee(employee);                 //
          }                                                                   //
          schedule.add(curShift);
          thisDayShifts.add(curShift);
        }
        thisWeekShifts.add(curShift);
      }
    }
    schedule.remove(0); // removing dummy shift :(
    return schedule;
  }

  /**
   * Override this!
   */
  public boolean validation() { return true; }

  private int dayOfWeek(int day) { return day - (day / 7) * 7; }
}
