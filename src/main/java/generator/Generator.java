package generator;

import model.ScheduleShift;

public class Generator extends Engine {
  private boolean filledPositions() {
    return !(curShift.getEmployees().size() >= curProfShift.getPositions());
  }

  private boolean filledHours() {
    return true;
  }

  private boolean sameDay() {
    for (ScheduleShift shift : thisDayShifts)
      if (shift.getEmployees().contains(curEmployee)) return false;
    return true;
  }

  private boolean nightToMorning() {
    return !(curShiftName == curProfessionShifts.get(0).getShiftName() &&
             prevShift.getEmployees().contains(curEmployee));
  }

  private boolean quota() {
    int countShifts = 0;
    for (ScheduleShift shift : thisWeekShifts)
      if (shift.getEmployees().contains(curEmployee)) ++countShifts;
    return (countShifts * 8) < curEmployee.getWeeklyHours();
  }

  private boolean isRequiredProfession() {
    return curEmployee.getProfession().equals(curProfessionId);
  }

  /**
   * Validate an employee for shift
   */
  @Override
  public boolean validation() {
    return filledPositions() && filledHours() && sameDay() && nightToMorning() && quota() &&
        isRequiredProfession();
  }
}
