package export;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import model.*;

public class Sheet extends Export {
  private StringBuffer buf = new StringBuffer(
      "name,surname,age" + System.getProperty("line.separator") + "my_name,my_surname,my_age");

  public void export() throws IOException {
    BufferedWriter writer = new BufferedWriter(new FileWriter(new File(CSV)));
    writer.write(buf.toString());
    writer.flush();
    writer.close();

    System.out.println("Content of StringBuffer written to File.");
  }
}
