package export;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import model.*;

public class Image extends Export {
  private String[] columnNames = {"name", "surame", "age"};
  private Object[][] data = {{"my_name", "my_surname", 0}};

  public void export() throws IOException {
    JTable table = new JTable(data, columnNames);
    table.setBounds(0, 0, 480, 640);
    JTableHeader tableHeaderComp = table.getTableHeader();
    int totalWidth = tableHeaderComp.getWidth() + table.getWidth();
    int totalHeight = tableHeaderComp.getHeight() + table.getHeight();
    BufferedImage tableImage =
        new BufferedImage(totalWidth, totalHeight, BufferedImage.TYPE_INT_RGB);
    Graphics2D g2D = (Graphics2D)tableImage.getGraphics();
    tableHeaderComp.paint(g2D);
    g2D.translate(0, tableHeaderComp.getHeight());
    table.paint(g2D);
    ImageIO.write(tableImage, "png", new File(IMG));
  }
}
