package main;

import java.util.Scanner;
import view.cli.Display;
import view.gui.homePage.*;

class Launch {
  private static int guiDisplay(String[] argv) {
    HomePage.run(argv);
    return 0;
  }

  private static int cliDisplay() {
    Display disp = new Display();
    Scanner scan = new Scanner(System.in);
    scan.useDelimiter("\n");
    disp.run(scan);
    scan.close();
    return 0;
  }

  public static void main(String[] argv) {
    if (argv.length == 0 || argv[0].equals("-gui"))
      System.exit(guiDisplay(argv));
    else if (argv[0].equals("-cli"))
      System.exit(cliDisplay());
    else
      System.exit(1);
  }
}
