package main;

import export.Image;
import export.Sheet;
import generator.Generator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;
import model.Bussiness;
import model.Employee;
import model.ScheduleShift;
import parser.BussinessParser;
import parser.EmployeeParser;

public class Controller {
  private static final Controller INSTANCE = new Controller();

  private ArrayList<Bussiness> bussinesses = new ArrayList<>();
  private ArrayList<Employee> employees = new ArrayList<>();
  private ArrayList<ScheduleShift> schedule = new ArrayList<>();

  private Controller() {
    bussinesses = new BussinessParser().parse();
    employees = new EmployeeParser().parse();
  }

  /**
   * Adds a bussiness to json file.
   *
   * @param newBussiness
   */
  public void addBussiness(Bussiness newBussiness) {
    new BussinessParser().saveData(newBussiness, bussinesses);
    bussinesses = new BussinessParser().parse();
  }

  /**
   * Get all of the bussinesses in the json file.
   *
   * @return arraylist of bussinesses
   */
  public ArrayList<Bussiness> getBussinesses() { return bussinesses; }

  /**
   * Remove an bussiness from the json file.
   *
   * @param id the bussiness id
   */
  public void rmvBussiness(UUID id) {
    ArrayList<Bussiness> newBussinesses = bussinesses;
    try {
      for (Bussiness bussiness : bussinesses) {
        if (bussiness.getId().equals(id)) {
          newBussinesses.remove(bussiness);
          break;
        }
      }
    } catch (Exception e) {
      System.out.println(e);
      return;
    }
    new BussinessParser().saveData(newBussinesses);
    bussinesses = new BussinessParser().parse();
  }

  /**
   * Adds an employee to json file.
   *
   * @param newEmployee
   */
  public void addEmployee(Employee newEmployee) {
    new EmployeeParser().saveData(newEmployee, employees);
    employees = new EmployeeParser().parse();
  }

  /**
   * Get all of the employees in the json file.
   *
   * @return arraylist of employees
   */
  public ArrayList<Employee> getEmployees() { return employees; }

  /**
   * Query for a single employee by it's name.
   *
   * @param term the name of the employee
   * @return the employee
   */
  public Employee getEmployeeByName(String term) {
    Employee resEmployee = null;
    for (Employee employee : employees) {
      if (term.equals(employee.getFirstName()) || term.equals(employee.getLastName()))
        resEmployee = employee;
    }
    return resEmployee;
  }

  /**
   * Remove an employee from the json file.
   *
   * @param id the employee id
   */
  public void rmvEmployee(UUID id) {
    ArrayList<Employee> newEmployees = employees;
    for (Employee employee : employees) {
      if (employee.getId().equals(id)) {
        newEmployees.remove(employee);
        break;
      }
    }
    new EmployeeParser().saveData(newEmployees);
    employees = new EmployeeParser().parse();
  }

  /**
   * Generate the schedule.
   */
  public void createSchedule(int days, Bussiness bussiness) {
    schedule = new Generator().generate(days, bussiness);
  }

  /**
   * Get the schedule.
   *
   * @return arraylist of shifts
   */
  public ArrayList<ScheduleShift> getSchedule() { return schedule; }

  /**
   * Export the schedule as csv sheet or png image.
   */
  public void exportSchedule(String type) {
    try {
      switch (type) {
        case "csv" -> {
          new Sheet().export();
          break;
        }
        case "png" -> {
          new Image().export();
          break;
        }
        default -> {
          System.out.println("Unexpected type recieved.");
          break;
        }
      }
    } catch (IOException ex) { System.out.println("Exception at export execution."); }
  }

  public static Controller getController() { return INSTANCE; }
}
