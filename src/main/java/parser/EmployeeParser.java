package parser;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import model.Employee;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class EmployeeParser implements Parser {
  private Random shuffler;

  private UUID id, profession;
  private String firstName, lastName;
  private int weeklyHours;

  /**
   * Read json file and return employees
   *
   * @return list of employees from json file
   */
  public ArrayList<Employee> parse() {
    ArrayList<Employee> employees = new ArrayList<>();
    try (FileReader reader = new FileReader(EMPLOYEES)) {
      JSONParser jsonParser = new JSONParser();
      JSONObject jsonObj = (JSONObject)jsonParser.parse(reader);
      JSONArray employeeList = (JSONArray)jsonObj.get("employees");
      for (int i = 0; i <= employeeList.size() - 1; i++) {
        JSONObject jsonObj2 = (JSONObject)employeeList.get(i);
        id = UUID.fromString(jsonObj2.get("id").toString());
        firstName = (String)jsonObj2.get("fname");
        lastName = (String)jsonObj2.get("lname");
        profession = UUID.fromString(jsonObj2.get("profession").toString());
        weeklyHours = (int)(long)jsonObj2.get("weeklyHours");
        employees.add(new Employee(id, profession, firstName, lastName, weeklyHours));
      }
    } catch (IOException | ParseException e) { System.out.println(e); }
    return employees;
  }

  /**
   * Save Employees to json file
   * @param employees
   */
  @SuppressWarnings("unchecked") // jsonsimple generic, hide type warnings
  public void saveData(ArrayList<Employee> employees) {
    JSONArray employeeJArray = new JSONArray();
    for (Employee anEmployee : employees) {
      JSONObject employeeJson = new JSONObject();
      employeeJson.put("id", anEmployee.getId().toString());
      employeeJson.put("fname", anEmployee.getFirstName());
      employeeJson.put("lname", anEmployee.getLastName());
      employeeJson.put("profession", anEmployee.getProfession());
      employeeJson.put("weeklyHours", anEmployee.getWeeklyHours());
      employeeJArray.add(employeeJson);
    }
    JSONObject finalJsonObj = new JSONObject();
    finalJsonObj.put("employees", employeeJArray);
    FileWriter file;
    try {
      file = new FileWriter(EMPLOYEES);
      file.write(finalJsonObj.toJSONString());
      file.close();
    } catch (IOException e) { e.printStackTrace(); }
  }

  /**
   * Save variation that adds Employee to Employees
   * and saves to json file
   * @param employee
   * @param employees
   */
  public void saveData(Employee employee, ArrayList<Employee> employees) {
    employees.add(employee);
    saveData(employees);
  }

  /**
   * Sort an employee list by their ids.
   *
   * @param employees
   * @return sorted list of employees
   */
  public ArrayList<Employee> sortById(ArrayList<Employee> employees) {
    List<UUID> ids = new ArrayList<UUID>();
    for (Employee anEmployee : employees) ids.add(anEmployee.getId());
    return Employee.sortById(ids, employees);
  }

  /**
   * @param employees
   * @return shuffled list of employees
   */
  public ArrayList<Employee> shuffleEmployees(ArrayList<Employee> employees) {
    int newIndex;
    ArrayList<Employee> randEmployees = new ArrayList<>();
    List<Integer> indexArray = new ArrayList<>();

    shuffler = new Random(System.currentTimeMillis());

    while (indexArray.size() < employees.size()) {
      newIndex = shuffler.nextInt(employees.size());
      if (!indexArray.contains(newIndex)) { indexArray.add(newIndex); }
    }

    for (int i = 0; i <= employees.size() - 1; i++) {
      randEmployees.add(employees.get(indexArray.get(i)));
    }
    return randEmployees;
  }
}
