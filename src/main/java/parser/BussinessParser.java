package parser;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import model.Bussiness;
import model.Profession;
import model.ProfessionShift;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class BussinessParser implements Parser {
  private Random shuffler;

  private UUID id;
  private String name;

  /**
   * Read json file and return bussinesses
   *
   * @return list of bussinesses from json file
   */
  public ArrayList<Bussiness> parse() {
    ArrayList<Bussiness> bussinesses = new ArrayList<>();
    try (FileReader reader = new FileReader(BUSSINESS)) {
      JSONParser jsonParser = new JSONParser();
      JSONObject jsonObj = (JSONObject)jsonParser.parse(reader);
      JSONArray bussinessList = (JSONArray)jsonObj.get("bussinesses");
      for (int i = 0; i <= bussinessList.size() - 1; ++i) {
        ArrayList<Profession> professions = new ArrayList<Profession>();
        JSONObject bussiness = (JSONObject)bussinessList.get(i);
        id = UUID.fromString(bussiness.get("id").toString());
        name = (String)bussiness.get("name");
        JSONArray professionList = (JSONArray)bussiness.get("professions");
        for (int k = 0; k <= professionList.size() - 1; ++k) {
          JSONObject professionObj = (JSONObject)professionList.get(k);
          UUID id = UUID.fromString(professionObj.get("id").toString());
          String name = (String)professionObj.get("name");
          ArrayList<String> shifts = Parser.toList((JSONArray)professionObj.get("shifts"));
          ArrayList<Integer> positions =
              Parser.toIntList((JSONArray)professionObj.get("positions"));
          ArrayList<Integer> hours = Parser.toIntList((JSONArray)professionObj.get("hours"));
          ArrayList<ProfessionShift> profShifts = new ArrayList<>(); //
          for (int l = 0; l <= shifts.size() - 1; ++l)               // collect shifts
            profShifts.add(new ProfessionShift(                      //
                shifts.get(l), positions.get(l), hours.get(l)));     // and add them
          professions.add(new Profession(id, name, profShifts));     // to the prof obj
        }
        bussinesses.add(new Bussiness(id, name, professions));
      }
    } catch (IOException | ParseException e) { System.out.println(e); }
    return bussinesses;
  }

  /**
   * Save Bussinesses to json file
   * @param bussinesses
   */
  @SuppressWarnings("unchecked") // jsonsimple generic, hide type warnings
  public void saveData(ArrayList<Bussiness> bussinesses) {
    JSONArray employeeJArray = new JSONArray();
    for (Bussiness aBussiness : bussinesses) {
      JSONObject employeeJson = new JSONObject();
      employeeJson.put("id", aBussiness.getId().toString());
      employeeJson.put("name", aBussiness.getName());
      JSONArray professionJArray = new JSONArray();
      for (Profession profession : aBussiness.getProfessions()) {
        JSONObject professionJson = new JSONObject();
        professionJson.put("id", profession.getId().toString());
        professionJson.put("name", profession.getName());
        ArrayList<String> shifts = new ArrayList<>();
        ArrayList<Integer> positions = new ArrayList<>();
        ArrayList<Integer> hours = new ArrayList<>();
        for (ProfessionShift professionShift : profession.getShifts()) {
          shifts.add(professionShift.getShiftName());
          positions.add(professionShift.getPositions());
          hours.add(professionShift.getHours());
        }
        professionJson.put("shifts", shifts);
        professionJson.put("positions", positions);
        professionJson.put("hours", hours);
        professionJArray.add(professionJson);
      }
      employeeJson.put("professions", professionJArray);
      employeeJArray.add(employeeJson);
    }
    JSONObject finalJsonObj = new JSONObject();
    finalJsonObj.put("bussinesses", employeeJArray);
    FileWriter file;
    try {
      file = new FileWriter(BUSSINESS);
      file.write(finalJsonObj.toJSONString());
      file.close();
    } catch (IOException e) { e.printStackTrace(); }
  }

  /**
   * Save variation that adds Bussinesse to Bussinesses
   * and saves to json file
   * @param bussiness
   * @param bussinesses
   */
  public void saveData(Bussiness bussiness, ArrayList<Bussiness> bussinesses) {
    bussinesses.add(bussiness);
    saveData(bussinesses);
  }

  /**
   * Sort an bussiness list by their ids.
   *
   * @param bussinesses
   * @return sorted list of bussinesses
   */
  public ArrayList<Bussiness> sortById(ArrayList<Bussiness> bussinesses) {
    List<UUID> ids = new ArrayList<UUID>();
    for (Bussiness aBussiness : bussinesses) ids.add(aBussiness.getId());
    bussinesses = Bussiness.sortById(ids, bussinesses);
    return bussinesses;
  }

  /**
   * @param bussinesses
   * @return shuffled list of bussinesses
   */
  public ArrayList<Bussiness> shuffleEmployees(ArrayList<Bussiness> bussinesses) {
    int newIndex;
    ArrayList<Bussiness> randEmployees = new ArrayList<>();
    List<Integer> indexArray = new ArrayList<>();

    shuffler = new Random(System.currentTimeMillis());

    while (indexArray.size() < bussinesses.size()) {
      newIndex = shuffler.nextInt(bussinesses.size());
      if (!indexArray.contains(newIndex)) { indexArray.add(newIndex); }
    }

    for (int i = 0; i <= bussinesses.size() - 1; i++) {
      randEmployees.add(bussinesses.get(indexArray.get(i)));
    }
    return randEmployees;
  }
}
