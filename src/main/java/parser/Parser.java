package parser;

import java.util.ArrayList;
import org.json.simple.JSONArray;

public interface Parser {
  static final String BUSSINESS = "./bussiness.json";
  static final String EMPLOYEES = "./employees.json";

  static ArrayList<Integer> toIntList(JSONArray jarray) {
    ArrayList<Integer> array = new ArrayList<>();
    if (jarray != null)
      for (int l = 0; l <= jarray.size() - 1; ++l)
        array.add(Integer.valueOf(jarray.get(l).toString()));
    return array;
  }

  static ArrayList<String> toList(JSONArray jarray) {
    ArrayList<String> array = new ArrayList<>();
    if (jarray != null)
      for (int l = 0; l <= jarray.size() - 1; ++l) array.add(jarray.get(l).toString());
    return array;
  }
}
