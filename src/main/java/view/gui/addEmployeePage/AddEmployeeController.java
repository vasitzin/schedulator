package view.gui.addEmployeePage;

import java.util.UUID;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import main.Controller;
import model.Employee;

public class AddEmployeeController {
  public Controller controller = Controller.getController();
  @FXML private TextField professionField, fNameField, lNameField, weeklyHoursField;
  public void addEmployee() {
    UUID profession = UUID.fromString(professionField.getText().trim());
    String firstName = fNameField.getText().trim();
    String lastName = lNameField.getText().trim();
    int weeklyHours = Integer.parseInt(weeklyHoursField.getText().trim());
    Employee employee = new Employee(profession, firstName, lastName, weeklyHours);

    controller.addEmployee(employee);
  }
}
