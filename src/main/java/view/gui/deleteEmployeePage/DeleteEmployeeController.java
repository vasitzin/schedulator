package view.gui.deleteEmployeePage;

import java.util.UUID;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import main.Controller;

public class DeleteEmployeeController {
  private static Controller controller = Controller.getController();
  @FXML private TextField idField;
  public void removeEmployee() {
    String id = idField.getText().trim();
    UUID ID = UUID.fromString(id);

    controller.rmvEmployee(ID);
  }
}
