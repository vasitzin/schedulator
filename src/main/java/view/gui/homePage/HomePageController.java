package view.gui.homePage;

import java.io.IOException;
import java.net.URL;

import java.util.Optional;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import main.Controller;
import view.gui.addEmployeePage.AddEmployeeController;
import view.gui.deleteEmployeePage.DeleteEmployeeController;
import javafx.scene.Node;

public class HomePageController {

  private final URL pathToAddEmployeesFXML = getClass().getResource("/view/gui/addEmployeePage/AddEmployee.fxml");
  private final URL pathToDeleteEmployeesFXML = getClass().getResource("/view/gui/deleteEmployeePage/DeleteEmployee.fxml");
  private final URL pathToShowAllEmployeesFXML = getClass().getResource("/view/gui/showAllEmployeesPage/ShowAllEmployees.fxml");

  public HomePageController(){}
    
  @FXML
  private BorderPane homePagePane;

  @FXML
  private BorderPane showAllPagePane;
  @FXML
  private ListView employeesList;

  @FXML
  private Button addEmployee;

  @FXML
  private Button deleteEmployee;

  @FXML
  private Button showAllEmployees;

    
  @FXML
  public void initialize(){
      addEmployee.setOnAction(e -> switchPages("addEmployee"));
      deleteEmployee.setOnAction(e -> switchPages("deleteEmployee"));
      showAllEmployees.setOnAction(e -> switchPages("showAllEmployees"));
  }


  @FXML
  public void switchPages(String choice){
    switch (choice) {
      case "addEmployee":
        switchToAddEmployeePage(pathToAddEmployeesFXML);
        break;
      case "deleteEmployee":
        switchToDeleteEmployeePage(pathToDeleteEmployeesFXML);
        break;
      case "showAllEmployees":
        showAllEmployeesPage(pathToShowAllEmployeesFXML);
      default:
        break;
    }
  }

    

            // ADD EMPLOYEE PAGE
  public void switchToAddEmployeePage(URL path) {
    Dialog<ButtonType> dialog = new Dialog<>();
    dialog.initOwner(homePagePane.getScene().getWindow());
    FXMLLoader loader = new FXMLLoader(path);
    try{
      dialog.getDialogPane().setContent(loader.load());
    }
    catch(IOException e){
      e.printStackTrace();
      return;
    }

    dialog.setTitle("Add Employee");
    dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
    dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);

    Optional<ButtonType> result = dialog.showAndWait();
    if(result.isPresent() && result.get() == ButtonType.OK){
      AddEmployeeController controller = loader.getController();
      controller.addEmployee();
    }
    else{
      System.out.println("Cancel pressed");
    }        
  }

  @FXML
  public void switchToDeleteEmployeePage(URL path) {
    Dialog<ButtonType> dialog = new Dialog<>();
    dialog.initOwner(homePagePane.getScene().getWindow());
    FXMLLoader loader = new FXMLLoader(path);
    try{
      dialog.getDialogPane().setContent(loader.load());
    }
    catch(IOException e){
      e.printStackTrace();
      return;
    }

    dialog.setTitle("Remove Employee");
    dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
    dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);

    Optional<ButtonType> result = dialog.showAndWait();
    if(result.isPresent() && result.get() == ButtonType.OK){
      DeleteEmployeeController controller = loader.getController();
      controller.removeEmployee();
    }
    else{
      System.out.println("Cancel pressed");
    }        
  }

  public void showAllEmployeesPage(URL path) {
    try {
      FXMLLoader loader = new FXMLLoader(path);
      Parent root = loader.load();
      Stage showAllStage = new Stage();
      showAllStage.setTitle("Show All Employees");
      Scene showAllScene = new Scene(root, 600, 400);
      showAllStage.setScene(showAllScene);
      showAllStage.show();
    }
    catch (IOException e){
      System.out.println("Can't load show all window");
      e.printStackTrace();
    }
  }
}
