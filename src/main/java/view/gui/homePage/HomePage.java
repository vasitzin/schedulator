package view.gui.homePage;

import main.Controller;
import java.io.IOException;
import javafx.stage.Stage;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import java.net.URL;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class HomePage extends Application{
    private static Controller ctl;
    
    public static void run(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage homePage) throws IOException{
        URL path = getClass().getResource("HomePage.fxml");
        FXMLLoader loader = new FXMLLoader(path);
        Parent root = loader.load();
        Scene scene = new Scene(root, 400, 600);
        homePage.setTitle("Scedulator App");
        homePage.setScene(scene);
        homePage.show();
    }
    
}
