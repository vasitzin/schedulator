package view.cli;

import java.util.Scanner;

public class Menu {
  public int menu(Scanner scan) {
    System.out.print("-----------{ MENU }-----------\n"
                     + "0-Exit Program\n"
                     + "1-This Menu\n"
                     + "2-Show all employees\n"
                     + "3-Show a specific employ\n"
                     + "4-Add employee\n"
                     + "5-Delete employee\n"
                     + "6-Show all bussinesses\n"
                     + "7-Add bussiness\n"
                     + "8-Delete bussiness\n"
                     + "9-Generate weekly schedule.\n"
                     + ">> ");
    int choice = scan.nextInt();
    return choice;
  }
}
