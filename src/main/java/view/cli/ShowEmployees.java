package view.cli;

import java.util.ArrayList;
import model.Employee;

public class ShowEmployees {
  public void showAll(ArrayList<Employee> employees) {
    System.out.println("--------< Employees >---------");
    for (Employee employee : employees) {
      System.out.println(employee.getId() + " | " + employee.getFirstName() + " " +
                         employee.getLastName() + ", " + employee.getProfession() + ", " +
                         employee.getWeeklyHours());
    }
  }
}
