package view.cli;

import java.util.Scanner;
import java.util.UUID;

public class Delete {
  public UUID deleteEmployee(Scanner scan) {
    System.out.println("----------< Delete >----------");
    System.out.print("Enter ID of the employee: ");
    UUID id = UUID.fromString(scan.next());
    return id;
  }

  public UUID deleteBussiness(Scanner scan) {
    System.out.println("----------< Delete >----------");
    System.out.print("Enter ID of the bussiness: ");
    UUID id = UUID.fromString(scan.next());
    return id;
  }
}
