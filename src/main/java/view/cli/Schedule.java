package view.cli;

import java.util.ArrayList;
import model.ScheduleShift;

public class Schedule {
  public void displaySchedule(ArrayList<ScheduleShift> schedule) {
    System.out.println("---------< Generate >---------");
    schedule.forEach((shift) -> {
      System.out.println(shift.getDayName() + ": " + shift.getShiftName());
      shift.getEmployees().forEach((employee) -> {
        System.out.print(employee.getLastName() + " " + employee.getFirstName() + ", ");
      });
      System.out.println();
    });
  }
}
