package view.cli;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.UUID;
import model.Bussiness;
import model.Employee;
import model.Profession;

public class Add {
  public Employee addEmployee(ArrayList<Employee> employees, Scanner scan) {
    System.out.println("------------< Add >-----------");
    System.out.print("Enter employees first name: ");
    String fname = scan.next();
    System.out.print("Enter employees last name: ");
    String lname = scan.next();
    System.out.print("Enter Weekly working hours: ");
    int weeklyHours = scan.nextInt();
    System.out.print("Enter profession: ");
    UUID profession = UUID.fromString(scan.next());
    return new Employee(profession, fname, lname, weeklyHours);
  }

  public Bussiness addBussiness(ArrayList<Bussiness> bussinesses, Scanner scan) {
    System.out.println("------------< Add >-----------");
    System.out.print("Enter bussiness name: ");
    String name = scan.next();
    return new Bussiness(name, new ArrayList<Profession>());
  }
}
