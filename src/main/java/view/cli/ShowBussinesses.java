package view.cli;

import java.util.ArrayList;
import model.Bussiness;
import model.Profession;

public class ShowBussinesses {
  public void showAll(ArrayList<Bussiness> bussinesses) {
    System.out.println("-------< Bussinesses >--------");
    for (Bussiness bussiness : bussinesses) {
      System.out.println(bussiness.getId() + " | " + bussiness.getName() + ", Professions:");
      for (Profession profession : bussiness.getProfessions()) System.out.println(
          "\t" + profession.getName() + ", Shifts: " + profession.getShifts());
    }
  }
}
