package view.cli;

import java.util.Scanner;
import java.util.UUID;
import main.Controller;
import model.Bussiness;
import model.Employee;

public class Display {
  private final Controller ctl = Controller.getController();
  private final int EXIT = 0;
  private final int MENU = 1;
  private final int SHOW_EMPLOYEES = 2;
  private final int SEARCH_EMPLOYEE = 3;
  private final int ADD_EMPLOYEE = 4;
  private final int DELETE_EMPLOYEE = 5;
  private final int SHOW_BUSSINESSES = 6;
  private final int ADD_BUSSINESS = 7;
  private final int DELETE_BUSSINESS = 8;
  private final int GENERATE = 9;
  private final int EXPORT_CSV = 21;
  private final int EXPORT_IMG = 22;

  private int bussinessChosen = 0;
  private int choice = MENU;
  private Boolean exit = false;

  public void run(Scanner scan) {
    while (!exit) { form(choice, scan); }
  }

  public void form(int choice, Scanner scan) {
    try {
      switch (choice) {
        case MENU -> {
          this.choice = new Menu().menu(scan);
          System.out.println("------------------------------");
          break;
        }
        case SHOW_EMPLOYEES -> {
          new ShowEmployees().showAll(ctl.getEmployees());
          System.out.println("------------------------------");
          this.choice = MENU;
          break;
        }
        case SEARCH_EMPLOYEE -> {
          String term = new Search().searchEmployee(scan);
          Employee resEmployee = ctl.getEmployeeByName(term);
          if (resEmployee == null) {
            System.out.println("Employee not found!");
          } else {
            System.out.println(resEmployee.getId()
                + " | " + resEmployee.getFirstName()
                + " " + resEmployee.getLastName()
                + ", " + resEmployee.getProfession()
                + ", " + resEmployee.getWeeklyHours());
          }
          System.out.println("------------------------------");
          this.choice = MENU;
          break;
        }
        case ADD_EMPLOYEE -> {
          Employee newEmployee = new Add().addEmployee(ctl.getEmployees(), scan);
          ctl.addEmployee(newEmployee);
          System.out.println("------------------------------");
          this.choice = MENU;
          break;
        }
        case DELETE_EMPLOYEE -> {
          UUID id = new Delete().deleteEmployee(scan);
          ctl.rmvEmployee(id);
          System.out.println("------------------------------");
          this.choice = MENU;
          break;
        }
        case SHOW_BUSSINESSES -> {
          new ShowBussinesses().showAll(ctl.getBussinesses());
          System.out.println("------------------------------");
          this.choice = MENU;
          break;
        }
        case ADD_BUSSINESS -> {
          Bussiness newBussiness = new Add().addBussiness(ctl.getBussinesses(), scan);
          ctl.addBussiness(newBussiness);
          System.out.println("------------------------------");
          this.choice = MENU;
          break;
        }
        case DELETE_BUSSINESS -> {
          UUID id = new Delete().deleteBussiness(scan);
          ctl.rmvBussiness(id);
          System.out.println("------------------------------");
          this.choice = MENU;
          break;
        }
        case GENERATE -> {
          ctl.createSchedule(8, ctl.getBussinesses().get(bussinessChosen));
          new Schedule().displaySchedule(ctl.getSchedule());
          System.out.println("------------------------------");
          this.choice = MENU;
          break;
        }
        case EXPORT_CSV -> {
          ctl.exportSchedule("csv");
          this.choice = MENU;
          break;
        }
        case EXPORT_IMG -> {
          ctl.exportSchedule("png");
          this.choice = MENU;
          break;
        }
        case EXIT -> {
          this.exit = true;
          break;
        }
      }
    } catch (java.util.InputMismatchException ex) {
      System.out.println("\033[1;91mAn integer is needed!\033[0m");
      System.out.println("------------------------------");
      scan.nextLine();
    } catch (Exception ex) {
      System.out.println("\033[1;91mOops...\n" + ex + ".\033[0m");
      System.out.println("------------------------------");
    } catch (Error er) {
      System.out.println("\033[1;91mOops...\n" + er + ".\033[0m");
      System.out.println("------------------------------");
    }
  }
}
